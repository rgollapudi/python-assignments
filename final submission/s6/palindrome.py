"""check whether the string is palindrome or not"""
def ispalindrome(data):
    """parameters
    -----
    data: enter the data to check it is palindrome or not
    
    conditions
    -----
    if type of data is not string or list:
        return False
    reverse the list using [::-1] operation
    if reverse==data:
        return True"""
        
    if type(data) != str and type(data) != list:
        return False
    if type(data)==str:
        data=data.lower()
    rev=data[::-1]
    if data == rev:
        return True
    return False
