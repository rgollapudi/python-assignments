# importin pandas 
import pandas as pd

"""function to add new columns which are sum of all marks and percentage of them"""
def add_columns():
	"""adding extra columns to the file
	
	...
	
	attributes
	------
	total: gives total sum of marks
	percentage: shows percentage of marks out of 300
	
	returns
	-----
	newfile with extra columns"""

	df = pd.read_csv(filepath)

	df["Total"] = df[['maths', 'science','arts']].sum(axis=1)
	df["percentage"]= df[["Total"]]/df[["Total"]].sum()
	print(df)
	df.to_csv(filepath)

	
# driver code
filepath = input("filepath: ")
# filepath = filename+'.csv' 
add_columns()

