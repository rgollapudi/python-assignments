# give an existed text file name
filename="reverse"
extension=".txt"
# reverse the file name
newfile=filename[::-1]
writeFile = open(newfile+extension, "w") 
# Open the input file and get the content into a variable data
with open(filename+extension, "r") as readFile: 
    txt = readFile.read() 
# reversing data in the file
Content = txt[::-1] 
writeFile.write(Content) 
writeFile.close()

