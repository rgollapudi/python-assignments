import random
import string

alphabets = list(string.ascii_letters)
digits = list(string.digits)
special_characters = list("!@#$%^&*()")
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()")



class Password:
    def generator(self):
        
        length = int(input("Enter password length: "))

        a = int(input("Enter number of alphabets in password: "))
        d = int(input("Enter number of digitsts in password: "))
        s = int(input("Enter number of spl characters in password: "))

        characters_count = a + d + s

       
        if characters_count > length:
            print("Please enter less number of character count")
            return


        password = []
        
        for i in range(a):
            password.append(random.choice(alphabets))


        for i in range(d):
            password.append(random.choice(digits))


        for i in range(s):
            password.append(random.choice(special_characters))


       
        if characters_count < length:
            random.shuffle(characters)
            for i in range(length - characters_count):
                password.append(random.choice(characters))


        
        random.shuffle(password)

   
        print("New generated password is : "+"".join(password))
            
    def strongness(self,*args):
        for x in args:
            n = len(x)

            L = False
            U = False
            D = False
            C = False
            normalChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "
            
            for i in range(n):
                if x[i].islower():
                    L = True
                if x[i].isupper():
                    U = True
                if x[i].isdigit():
                    D = True
                if x[i] not in normalChars:
                    C = True
        
            print("Strength of password for "+x+"is : ", end = "")
            if (L and U and
                D and C and n >= 8):
                print("Strong")
                
            elif ((L or U) and
                C and n >= 6):
                print("Moderate")
            else:
                print("Weak")


res = Password()
res.generator()
t = int(input("Enter number of cases : "))
lis = []
for i in range(t):
    s = input("enter password : ")
    lis.append(s)
for i in lis:
    res.strongness(i)