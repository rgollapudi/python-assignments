# creating class node  
class Node:
  """function to initialise node object"""
  def __init__(self,data):
    self.data = data
    self.next = None
# create an instance object for head node
class Linkedlist:
  """function to initialise linkedlist object"""
  def __init__(self):
    self.head = None 
# logic for inserting node at beginning  
  def insertatbeg(self,data):
    """insert new node at beginning
    
    attributes
    -----
    new_node: new value we passed
    new_node.next: next value to the new node"""

    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
#  logic for inserting node at end 

  def insertatend(self, new_data):
        """insert new node at end
    
        attributes
        -----
        new_node: new value we passed
        last: assume as head node

        conditions
        -----
        if head is empty:
          returns new_node
          
        loop
        -----
        while last.next:
          return last as last.next
        last.next= new_node
        """
        new_node = Node(new_data)
        if self.head is None:
            self.head = new_node
            return
        last = self.head
        while (last.next):
            last = last.next
        last.next = new_node

# logic for inserting node at particular position   
  def insertAfter(self, prev_node, new_data):
        """insert new node at specific position
    
        attributes
        -----
        new_node: new value we passed
        new_node.next: next value to the new node
        prev_node.next: new value
        """

        if prev_node is None:
            print("The given previous node must inLinkedList.")
            return
        new_node = Node(new_data)
        new_node.next = prev_node.next
        prev_node.next = new_node

# logic for deleting node at beginning  
  def deleteatbeg(self):
    """delete node at beginning
    
    attributes
    -----
    temp: root value
    
    returns
    -----
    new list from second node"""

    temp = self.head 
    self.head = temp.next 
    temp.next = None
# logic for deleting node at end  
  def deleteatend(self):
    """delete node at end
    
    attributes
    -----
    prev: root node
    temp: next to root node
    
    loop
    -----
    while next value to temp is not none:
      temp.next is temp
      prev.next is prev

    returns
    -----
    last element is none
    """

    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
# logic for deleting node at particular position  
  def deleteatposition(self,pos):
    """delete node at specific position
    
    attributes
    -----
    prev: root node
    temp: next to root node
    
    loops
    -----
    traverse through the node at the given position
    and deletevthe node at that position
    """

    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
#  displaying the data in linkedlist  
  def display(self):
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      new = ""
      while(temp):
        new+=temp.data
        temp=temp.next 
      return new
# check if expression is balanced or not
def is_balanced(expr):
	stack = []
	# Traverse the Expression
	for char in expr:
		if char in ["(", "{", "["]:
			stack.append(char)
		else:
			if not stack:
				return False
			curr_char = stack.pop()
			if curr_char == '(':
				if char == ")":
					return True
			if curr_char == '{':
				if char == "}":
					return True
			if curr_char == '[':
				if char == "]":
					return True
	if stack:
		return False
	return True

l = Linkedlist()
n = Node('(')
l.head = n
n1 = Node('{')
n.next = n1 
n2 = Node('}')
n1.next = n2 
# n3 = Node(')')
# n2.next = n3 

# l.insert_at_begining('[')
l.insertatend(')')
# l.delete_at_end()
x = l.display()

expression = x
print(expression)

#call the function
if is_balanced(expression):
	print("Balanced")
else:
	print("Not Balanced")
