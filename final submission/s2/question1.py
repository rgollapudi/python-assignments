# creating class node  
class Node:
  """function to initialise node object"""
  def __init__(self,data):
    self.data = data
    self.next = None
# create an instance object for head node
class Linkedlist:
  """function to initialise linkedlist object"""
  def __init__(self):
    self.head = None 
# logic for inserting node at beginning
  def insertAtFirst(self,data):
    """insert new node at beginning
    
    attributes
    -----
    new_node: new value we passed
    new_node.next: next value to the new node"""

    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
#  logic for inserting node at end
  def insertAtEnd(self,data):
    """insert new node at end

    attributes
    -----
    new_node: new value we passed
    last: assume as head node

    conditions
    -----
    if head is empty:
      returns new_node
      
    loop
    -----
    while last.next:
      return last as last.next
    last.next= new_node
    """
    new_node = Node(data)
    if self.head is None:
        self.head = new_node
        return
    last = self.head
    while (last.next):
        last = last.next
    last.next = new_node

# logic for inserting node at particular position 
  def insertAtPos(self,pos,data):
    """insert new node at specific position
    
        attributes
        -----
        new_node: new value we passed
        new_node.next: next value to the new node
        temp.next: new value
        """

    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node




# logic for deleting node at beginning 
  def deleteAtStart(self):
    """delete node at beginning
    
    attributes
    -----
    temp: root value
    
    returns
    -----
    new list from second node"""

    temp = self.head 
    self.head = temp.next 
    temp.next = None
# logic for deleting node at end 
  def deleteAtEnd(self):
    """delete node at end
    
    attributes
    -----
    prev: root node
    temp: next to root node
    
    loop
    -----
    while next value to temp is not none:
      temp.next is temp
      prev.next is prev

    returns
    -----
    last element is none
    """

    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None

# logic for deleting node at particular position 
  def deleteAtPos(self,pos):
    """delete node at specific position
    
    attributes
    -----
    prev: root node
    temp: next to root node
    
    loops
    -----
    traverse through the node at the given position
    and deletevthe node at that position
    """
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next

  def print(self):
    if self.head is None:
      print("null")
    else:
      temp = self.head 
      while(temp):
        print(temp.data , end = " ")
        temp=temp.next
 
# driver code
l = Linkedlist()
n = Node(1)
l.head = n
n1 = Node(2)
n.next = n1 
n2 = Node(3)
n1.next = n2 
n3 = Node(4)
n2.next = n3 
l.insertAtFirst(3)
l.insertAtEnd(1)
l.insertAtPos(2,3)
l.deleteAtStart()
l.deleteAtEnd()
l.deleteAtPos(2)
l.print()
